For my final project I wanted to find out which rapper, 
out of a list of my favorite rappers, made shout-outs to their
hometown and other cities in their songs. In order to do this
I defined a class named Artist where each rapper is an instance of
the class Artist. When I invoked each instance the class would use
an API code from Musix Match to grab and return 10 track id codes
from Musix Match online for each rapper; next, the class uses those
10 returned track id codes and grabs and returns a list of 10 dictionaries 
which each contain information about each track and one of those keys
is a snippet of the lyrics. I also defined other functions outside 
of the class where I used an accumulator to create a dictionary
of how many times each word was mentioned. I used this dictionary
and sorted the keys to find which words were mentioned most frequently.
Once those keys were sorted in numerical order I ran a function
where I compared to see if any of the words in the lyrics were cities
I created in a text document mycity.txt. The cities mentioned are already
sorted in numerical order and I printed out the information based on
each rappers song city count data. 