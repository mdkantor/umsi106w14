import urllib
import urllib2
import json
import sys
import test

def pretty(obj):
    return json.dumps(obj, sort_keys=True, indent=2)

city = []
f = open('mycity.txt', 'r') 
for aline in f:
	home=aline.split()
	for zz in home:
		city.append(zz)
f.close()

apiKey = "a2957543a12a372bbc5f2927eef4cd53"
baseUrl = "http://api.musixmatch.com/ws/1.1/"

class Artist:
	def __init__(self,name):
		self.name=name
		self.tracks = self.get_tracks()
		self.tracks = self.get_all_lyrics(self.tracks)	
	def get_tracks(self):
		"""This takes name and runs it on musixmatch online to return a list of 10 track names"""
		tracks=[]
		api = 'track.search'
		params = urllib.urlencode({
			'apikey':apiKey,
			'q_artist':self.name,
			'f_has_lyrics':1,
			'format':'json'
			})
		url = baseUrl + api + "?" + params
		result = urllib2.urlopen(url)
		content = result.read()
		data = json.loads(content)
		for track in data['message']['body']['track_list']:
			tracks.append(track['track'])
		return tracks
	def get_lyrics(self,track):
		"""This takes the track_id codes from the dictionary pulled from online in get_tracks,
		it runs those track_id codes on musixmatch online and returns a list of dictionaries where each dictionary is info
		about each track (including the lyrics)"""
		api = 'track.lyrics.get'
		params = urllib.urlencode({
			'apikey':apiKey,
			'track_id':track['track_id']
			})
		url = baseUrl + api + "?" + params
		result = urllib2.urlopen(url)
		content = result.read()
		ldata = json.loads(content)
		return ldata['message']['body']['lyrics']['lyrics_body']
	def get_all_lyrics(self,tracks):
		"""This calls on the get_lyrics fxn and it extracts the ['lyrics'] from each of the tracks
	 	in the list of 10 tracks and it appends them to a list of just the track lyrics"""
		new_tracks = []
		for track in tracks:
			lyrics = self.get_lyrics(track)
			track['lyrics'] = lyrics
			new_tracks.append(track)
		return new_tracks 

def work(rapper):
	for song in rapper.tracks:
		s0=rapper.tracks[0]['lyrics'].split()
		s1=rapper.tracks[1]['lyrics'].split()
		s2=rapper.tracks[2]['lyrics'].split()
		s3=rapper.tracks[3]['lyrics'].split()
		s4=rapper.tracks[4]['lyrics'].split()
		s5=rapper.tracks[5]['lyrics'].split()
		s6=rapper.tracks[6]['lyrics'].split()
		s7=rapper.tracks[7]['lyrics'].split()
		s8=rapper.tracks[8]['lyrics'].split()
		s9=rapper.tracks[9]['lyrics'].split()
	all_lyrics= s0+s1+s2+s3+s4+s5+s6+s7+s8+s9
	return all_lyrics

def lyrics(rapper):
	words={}
	for w in work(rapper):
		if w in words:
			words[w]=words[w]+1
		else:
			words[w]=1
	return words

def in_order(rapper):
	D=lyrics(rapper)
	sorted_words=sorted(D.items(), key=lambda x: x[1], reverse=True)
	return sorted_words

def FORMYCITY(rapper):
	shoutout=[]
	dic=lyrics(rapper)
	Z=dic.keys()
	for w in Z:
		if w in city:
			shoutout.append(w)
		else:
			pass
	return shoutout
#############################################
artist1= ' Jay-Z '
JZ= Artist(artist1)
artist2= ' Big Sean'
BS= Artist(artist2)
artist3= ' Kendrick Lamar '
KDOT= Artist(artist3)


my_fave= [JZ, BS, KDOT]

rap_battle=[]
top_cities=map(FORMYCITY, my_fave)

top_JZ=[]
for x in FORMYCITY(JZ):
	top_JZ.append(x.encode(sys.stdout.encoding, 'replace'))
#print top_JZ
for a in top_JZ:
	print "In his lyrics Jay-Z gives shout-outs to %s" % (a)
print "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
top_BS=[]
for x in FORMYCITY(BS):
	top_BS.append(x.encode(sys.stdout.encoding, 'replace'))
#print top_BS
for b in top_BS:
	print "In his lyrics Big Sean gives shout-outs to %s" % (b)
print "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
top_KDOT=[]
for c in FORMYCITY(KDOT):
	top_KDOT.append(x.encode(sys.stdout.encoding, 'replace'))
#print top_BS
for city in top_KDOT:
	print "In his lyrics Kendrick Lamar gives shout-outs to %s" % (c)
print "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"



test.testEqual(type(work(JZ)), list)
test.testEqual(type(FORMYCITY(JZ)), list)
test.testEqual(type(lyrics(JZ)), dict)
test.testEqual(type(in_order(JZ)), list)
test.testEqual(type(FORMYCITY(JZ)), list)










