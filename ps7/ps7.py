import test

###### (3 points) String Interpolation Questions############

# convert this string interpolation to one using only the + operator, not %
# make t equal to the same thing as s, which should 
x = 12
y = 4
s = "You have $%d. If you spend $%d, you will have $%d left" % (x, y, x-y)
# fill in the next line to generate the same string using only the + operator, no 
t = "You have $" + str(x) + ". If you spend $" + str(y) + ", you will have $" + str(x-y) + " left" 
test.testEqual(t, s, "convert string interpolation to one using only the + operator, not %")

# convert this string concatenation to one using string interpolation
x = 12
fname = "Joe"
our_email = "scammer@dontfallforthis.com"
s = "Hello, " + fname + ", you may have won $" + str(x) + " million dollars. Please send your bank account number to " + our_email + " and we will deposit your winnings."
t = "Hello, %s, you may have won $%d million dollars. Please send your bank account number to %s and we will deposit your winnings." % (fname, x, our_email)
test.testEqual(t, s, "convert string concatenation to one using string interpolation")

# Given the code below (you should be using what we provide!) write more code to produce the string 
# "Albert walked 0.67 miles today in 50 minutes." Print that string.
nm = "Albert"
min_amt = 50
mile_amt = 0.673892
print "%s walked %0.2f miles today in %d minutes" % (nm, mile_amt, min_amt)






# Define a function called walk_reporter, which takes as input: 
#  a string that represents someone's name, 
#  a float that represents the number of miles they walked,
#  and an integer that represents the number of minutes they spent walking.
#
# The function should RETURN a string in the format:
# "[NAME STR] walked [MILE FLOAT with TWO digits after the decimal] miles in [MINUTES INT] minutes."
# You MUST use string interpolation in the function. 
# You should NOT use raw_input to get the inputs; they are passed in as parameters.

def walk_reporter(nm, miles, min):
	return "%s walked %0.2f miles in %d minutes" % (nm, miles, min)
	





test.testEqual(walk_reporter("Jamie",5.233679,202), "Jamie walked 5.23 miles in 202 minutes", "walk_reporter test 1")
test.testEqual(walk_reporter("Pythagoras",3.1415926,314),
"Pythagoras walked 3.14 miles in 314 minutes", "walk reporter test 2")

#######(3 points) Sorting Questions#########
"""
###According to the documentation of the sorted function-- at http://docs.python.org/2.7/library/functions.html#sorted -- the four parameter names are:
# -- iterable
# -- cmp
# -- key
# -- reverse 

L = ["First", "b", "two"]
# convert the line below to a call to the function sorted() which does not 
# use any keywords, but instead passes arguments by position (remember the keyword chapter you've read this week)
L2 = sorted(L, key = lambda x: len(x))
L3 = L #sorted # change this line so it calls the sorted function with positional arguments, not keyword arguments
test.testEqual(L2, L3, "convert this call to sorted to one that doesn't use keyword parameters")

# convert this next line to a simpler call to the sorted() function 
#   which uses keyword parameters to specify what parameter is which
L4 = sorted(L, None, None, True)
L5 = L
test.testEqual(L4, L5, "Convert this call to a simpler one using keyword parameters and only passing L and True, omitting the other two.")

# sort this list based on the value in the ones digit
nl = [6, 78, 89, 73, 24]
L6 = nl # fix this line to make the test pass
test.testEqual(L6, [73, 24, 6, 78, 89], "sort based on the value in the ones digit")
"""
###### (2 points + 1 bonus point opportunity) Outputting in .csv format#######

#  There is a file called fb_data2.txt in the cTools resource area. (We've posted it there rather than distributing it through bitbucket to preserve confidentiality: only students in the class can see this data.) Copy that file into the ps7 folder on your computer, the same directory where you have this file, ps7.py.

#. (a) Write code to open the file, and use it to create a dictionary where the keys are people's names, 
#      and the values of the keys are the number of times each person wrote something on the Facebook
#      group. For example, {"Jackie C.": 8, "Nick R.": 5}. 
#      (Note that those are NOT real numbers -- just an example of what the key-value pairs 
#      should look like.)
#      HINT: Look at problem set 3, question 3, in Week 4. This is a repeat of that, except that you have to read the data from a file instead of copying it into a string.
m= open("fbdata2.txt", "r")
n=m.readlines()

posters={}
for ln in n:
    if ln[:5] == 'from:':
        name = ln[6:].lstrip()
        if name not in posters:
            posters[name] = 1
        else:
            posters[name] = posters[name] + 1
for p in posters:
    print "%s posted %d times" % (p,posters[p])
#.     For each person who posted, print a string that looks like:
#      <PERSON'S NAME> posted <NUMBER> times. 
#	   Use string interpolation, NOT string concatenation.
"""
"%s posted %d times" % (name, 
def walk_reporter(nm, miles, min):
	return "%s walked %0.2f miles in %d minutes" % (nm, miles, min)"""
#  (b) Now do what you did in (a), except instead of PRINTING the strings to the console,
#      create a new file called "post_count_strings.txt" and write each "<NAME> posted <NUMBER> times"
#      string to that file, SORTED ALPHABETICALLY by first name.
#      You should end up with a file that looks like, FOR EXAMPLE:
infile= open("fbdata2.txt", "r")
outfile= open("post_count_strings.txt", "w")

def printabc(posters, reverse=False, by_value=False):
	pairs=posters.items()
	if by_value:
		if reverse:
			s=sorted(pairs, None, lambda x: x[1], True)
		else:
			s=sorted(pairs, None, lambda x: x[1])
	else:
		if reverse:
			s=sorted(pairs, None, lambda x:x[0], True)
		else:
			s=sorted(pairs, None, lambda x: x[0])
	for (k,v) in s:
		print "%s printed %d times" % (k, v)




outfile.write(printabc(posters))

infile.close()
outfile.close()  

new= open("post_count_strings.txt", "r")   
print new
new.close()
#      Jackie C. posted 4 times.
#      Nick R. posted 5 times.
#  
#      HINTS: 
#           -- check the online chapter on Files, the last section on writing to files.  
#           -- You will need to open your new file in write mode ('w')
#           -- Instead of print, you will write something like f.write(...)
#           -- Don't forget to close the file when you're done with it

#  (c) Now create a new .CSV file, and write to the file so you have a column of 
#      names, and a column of post counts. 

#      (If you view this in Excel or an open word processing
#      program, you'd see in one cell a name, in the cell to its right, the number of times 
#      that person posted, etc.) If you view this just in a text editor, you'll see something
#      that looks like:
#
#      Jackie C.,4
#      Nick R.,5 



#  (d) With the .CSV file you've created, use Excel or Google Drive to make a chart
#      that represents the post counts. You might want to make a bar chart or another type
#      of plot -- up to you, as long as it represents the post count data! Save that chart
#      as an image or pdf in the folder with your homework.

#  (e) Post an image of your chart in the Facebook group!

### ---

# (Even if you don't want to do this, READ ON till the end of this file! 
# There are more problems that deal with the Shannon game code below.)

# Bonus point opportunity: 
# Do something neat with our Facebook data,
# with your newfound knowledge about creating .CSVs 
# and any of the other stuff we've learned so far.
# This can take any form and is not required, but might be an interesting 
# way to start thinking about all the neat stuff you can already do with 
# data management + Python.


### ---

######(2 points, + 1 bonus point opportunity): Code understanding with the Shannon game solution set#########

#### Below is a slightly modified version of the solution set for the Shannon game. 
#### A bunch of test.testEqual and test.testType calls have been inserted. 
#### In some cases you have to set the second parameter of those test function calls, to make the tests pass.
#### In other cases, you have to write a little code before it to make the tests pass.
#### This means you'll have to read through the following solution set code, and
#### write or change a little bit of code or comments where you see a comment that says "task:"!
#### The comments that DON'T say "task" are comments to inform you of what's going on in the code.

#### At the bottom is another challenge problem, if you found this code walking easy.
    
import random

def letter_frequencies(txt):
    d = {}
    for c in txt:
        if c not in d:
            d[c] = 1
        elif c in alphabet:
            d[c] = d[c] + 1
        # don't bother tracking letters that aren't in our alphabet
    return d

def guess(prev_txt, guessed_already):
    # guess a letter randomly
    idx = random.randrange(0, len(alphabet))
    return alphabet[idx]    


def guess_no_dup(prev_txt, guessed_already):
    # guess a letter randomly until you find one that hasn't been guessed yet
    while True:
        idx = random.randrange(0, len(alphabet))
        candidate =  alphabet[idx]
        if candidate not in guessed_already:
            return candidate     

def keys_sorted_by_value(d):
    in_order = sorted(d.items(), None, lambda x: x[1], True)
    res = []
    for (k, v) in in_order:
        res.append(k)
    return res

def guess_by_frequency(prev_txt, guessed_already):
    # return the best one that hasn't been guessed yet
    for let in keys_sorted_by_value(overall_freqs):
        if let not in guessed_already:
            return let
    return None # No unguessed letters left; shouldn't happen!   
    
def game(txt, feedback=True, guesser = guess):
    """Plays one game"""
    # accumulate the text that's been revealed
    revealed_text = ""
    
    # accumulate the total guess count
    total_guesses = 0
    # accumulate the total characters to be guessed
    total_chars = 0
    
    # Loop through the letters in the text, making a guess for each
    for c in txt:
        if c in alphabet: # skip letters not in our alphabet; don't have to guess them
            total_chars = total_chars + 1
            # accumulate the guesses made for this letter
            guesses = ""
            guessed = False
            if feedback:
                print "guessing " + c,
            while not guessed:
                # guess until you get it right
                g = guesser(revealed_text, guesses)
                guesses = guesses + g
                if g == c:
                    guessed = True
                if feedback:
                    print g, 
            
            total_guesses = total_guesses + len(guesses)
            revealed_text = revealed_text + c
            if feedback:
                print(str(len(guesses)) + " guesses ")

    return total_chars, total_guesses

    
# note, the last two characters are the single quote and double quote. They are
# escaped, writen as \' and \", similar to how we have used escaping for tabs, \t,
# and newlines, \n.
alphabet = " !#$%&()*,-./0123456789:;?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[]abcdefghijklmnopqrstuvwxyz\'\""
caps = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

f = open('train.txt', 'r')
overall_freqs = letter_frequencies(f.read())
caps_freqs = {}
for c in caps:
    if c in overall_freqs:
        caps_freqs[c] = overall_freqs[c]
sorted_caps = keys_sorted_by_value(caps_freqs)
heuristics = {'q':{'priority': 1, 'guesses':['u', 'a']}, 
              '. ':{'priority': 2, 'guesses': sorted_caps}}
txt1 = "Question. Everything."
txt2 = "Try to guess as Holmes would what the next letter will be in this quite short text. Now is the time."

run_tests = True
def heuristic_guesser(prev_txt, guessed_already):
    global run_tests
    if run_tests:
        # task: fill these in to show you know what the types of these things are
        # replace the "??" string with a string that describes the type, like "int"
        test.testType(prev_txt, "??","test 1 in heuristic_guesser")
        test.testType(guessed_already,"??", "test 2 in heuristic_guesser")
        test.testType(heuristics, "??"," test 3 in heuristic_guesser")
        test.testType(heuristics.items(), "??", "test 4 in heuristic_guesser")
        test.testType(lambda x: x[1]['priority'], "??", "test 5 in heuristic_guesser")
    
    sorted_heuristics = sorted(heuristics.items(), None, lambda x: x[1]['priority'], False)
    # task: make an equivalent call to sorted using keyword arguments instead of positional arguments
    new_sorted_heuristics = []
    if run_tests:
        test.testEqual(sorted_heuristics, new_sorted_heuristics, "test 6 in heuristic_guesser")
    for key,value in sorted_heuristics:
        if run_tests:
            test.testType(key, "??", "type of key in heuristic_guesser")
            test.testType(value, "??", "type of value in heuristic_guesser")
        key_length = len(key)
        if len(prev_txt) >= key_length and prev_txt[-key_length:].lower() == key:
            for l in value['guesses']:
                if l not in guessed_already:
                    return l
    # task: Write a comment explaining why the next line is needed. If you can't figure it out from the code, try adding a print statement after this comment so you'll see the values of prev_txt, guessed_already, and sorted_heuristics when that line of code is executed.
    if run_tests:
        test.testEqual(1, 2, "Comment out this test once you do the task described on the line above")
    run_tests = False   # only run the tests one time
    return guess_by_frequency(prev_txt, guessed_already)
    
def add_word(w, pri = 2):
    """Takes a word w as input and adds all its prefixes to the 
    heuristics dictionary"""
    for i in range(len(w)-1):
        prefix = w[:i+1]
        next_let = w[i+1]
        heuristics[prefix] = {'priority' : pri, 'guesses':[next_let]}
    heuristics[w] = {'priority' : pri, 'guesses':[' ', '.', ',']}

# task: What value is returned by the add_word function? 
# Replace the "??" string with the correct value that the add_word function returns in this test case.
test.testEqual(add_word("and"), "??", "What value is returned by the add_word function?")
    
f = open('train.txt', 'r')
train = f.read()
f.close()

f= open('test.txt', 'r')
test_txt = f.read()
f.close()


res1 = game(test_txt, False, heuristic_guesser)

# Now use the text in the variable train to calculate the most frequent words in that text. Only consider words that have more than 4 letters.
# Call the add_word function on each of the 20 most frequent words.
# Then see how much your heuristic_guesser has improved.

words = {}
for w in train.split():
    if len(w) >=4:
        if w not in words:
            words[w] = 1
        else:
            words[w] = words[w] + 1

sorted_words = sorted(words.items(), None, lambda x: x[1])

# task: how many entries are added to the heuristics dictionary from the calls to add_word below?
# Write the answer in a comment here.
start_count = len(heuristics.keys())
for (word, count) in sorted_words[:20]:
    add_word(word)
end_count = len(heuristics.keys())
test.testEqual(end_count - start_count, -1, "how many entries are added to the heuristics dictionary from the calls to add_word?")

res2 = game(test_txt, False, heuristic_guesser)

def output_result(min_guesses, actual_guesses):
    print "Minimum guesses: %d\tActual guesses: %d" % (min_guesses, actual_guesses)

output_result(res1[0], res1[1])
output_result(res2[0], res2[1])

print "The improved game made %d less guesses. " % (res1[1] - res2[1])



##### More code understanding (1 bonus point). Available only to students who scored 75 or below on the midterm.

## You have already filled in the tests that check your understanding of the heuristic_guesser code.
## Add at least five calls to test.testEqual and/or test.testType that demonstrate your
## understanding of the game function.

##### Challenge task (1 bonus point). Available to anyone.

# Write code to reduce the number of guesses needed. No fair using test.txt to rig it. But feel free to use train.txt. Two ideas you might try to implement:
# 1) Try to generalize from the idea of guessing u or a after q. 
# Use the training text to calculate, after any letter, which other letters are most frequent. 
# Then add heuristics for them. Try giving those heuristics higher or lower priority than the ones for the most common words, and see which is better.
# 2) Try to guess what word is likely to come next, based on the word that just occurred. Use the training text to find common word pairs. 