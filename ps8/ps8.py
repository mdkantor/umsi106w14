###### Homework 8 Exercises
import test
import urllib
import urllib2
import json

def pretty(obj):
    return json.dumps(obj, sort_keys=True, indent=2)

#### Exercises: A RESTful API

# The FAA has put out a REST API for accessing current information about US airports. 
# You'll be using it in the following exercises.

# First, point your web browser to the following URL:
#   http://services.faa.gov/airport/status/DTW?format=json
#
# The text that is shown in your browser is a JSON-formatted dictionary.
# It can easily be converted into a python dictionary and processed in a 
# manner similar to what we have done with the Facebook feed previously.
# The exercise below guides you through the process of writing python
# code that uses this RESTful API to extract information about some
# airports.


## Encoding query parameters in a URL
# (1a) Use urllib.urlencode() to generate the query parameter string, with one
#      parameter: 'format', whose value should be 'json'. 
#      (Just like in the URL you pointed your browser to above.)
#      Store the query parameter string in a variable called param_str

# put your code here
d= {'format': 'json'}
param_str = str(urllib.urlencode(d))

# (1b) Add (concatenate) the airport and the param_str to the base URL, which is:
#        http://services.faa.gov/airport/status/
#      Store the string in a variable called airport_request.
print '---------------'
baseurl = 'http://services.faa.gov/airport/status/'
airport = 'DTW'
airport_request = baseurl + airport + "?" + param_str
print airport_request

## TESTS - Problem 1
test.testEqual(param_str, "format=json", "testing correct output for 1(a)")
test.testEqual(airport_request,"http://services.faa.gov/airport/status/DTW?format=json", "testing correct output for 1(b)")



## Grabbing data off the web
# (2)  Use urllib2.urlopen() retrieve data from the address airport_request.
#      Store the data in a string called airport_json_str.
print '---------------'

result = urllib2.urlopen(airport_request)
airport_json_str = result.read()
print airport_json_str

# print airport_json_str # if you're curious what you got back

## TESTS - Problem 2
test.testType(airport_json_str, "string", "testing type in problem 2")
test.testEqual(len(airport_json_str), 498, "testing aspect of result string in problem 2")


## Converting a JSON string to a dictionary
# (3)  Use json.loads() to convert airport_json_str into a dictionary.
#      Store the dictionary in a variable called airport_data.
#      Then, print the dictionary, using the pretty function that we defined for you above, 
#      to turn it into a nicely indented format
print '---------------'
    
# put your code here!
airport_data=json.loads(airport_json_str)
print pretty(airport_data) # UNCOMMENT this line when you've written your code to see a prettily structured version of your json data

## TESTS - Problem 3
test.testType(airport_data,"dictionary","testing type of airport_data in problem 3")


## Extracting relevant information from a dictionary
# (4)  From the airport data, extract the name, the reason field from within the status, the
# current temperature, and the last time it was updated.
# HINT: Look at the nested data chapter.
# Save these in variables called, respectively: 
#   airport_name, status_reason, current_temp, recent_update

print '---------------------------------'

# put your code here
#print pretty(airport_data.keys())
print pretty(airport_data.values())

airport_name= str(airport_data['name'])
status_reason=str(airport_data['status'])
current_temp=str(airport_data['weather']['temp'])
recent_update=str(airport_data['weather']['meta']['updated'])

## Uncomment all the following lines when you're done:
print airport_name
print status_reason
print current_temp
print recent_update

## TESTS - PROBLEM 4
test.testType(airport_name,"string","testing type in p4 - airport_name")
test.testType(status_reason,"string","testing type in p4 - status_reason")
test.testType(current_temp, "string", "testing type in p4 -- current_temp")
test.testType(recent_update,"string","testing type in p4 -- recent_update")

print '---------------------------------'

## Generalizing your code
# (5a) Write a function called get_airport() that accepts a three-letter airport
# code and returns a data dictionary like the one you get in Problem 3.  Uncomment out the test line.

def get_airport(xxx):
	baseurl = 'http://services.faa.gov/airport/status/'	
	xxxurl= baseurl + xxx + "?" + param_str
	open_xxxurl=urllib2.urlopen(xxxurl)
	content_xxxurl=open_xxxurl.read()
	d_xxxurl=json.loads(content_xxxurl)
	return d_xxxurl

# put your code here

# The following line of code tests get_airport():
print pretty(get_airport('DTW'))
print pretty(get_airport('DCA'))

print '---------------------------------'

# (5b) Write another function called print_airport() that accepts an airport name
#      and prints out the info as in exercise 4.
#      This function should call get_airport().  Uncomment the test code to try it out.
def print_airport(IDK):
	r=get_airport(IDK)
	airport_name= str(r['name'])
	status_reason=str(r['status'])
	current_temp=str(r['weather']['temp'])
	recent_update=str(r['weather']['meta']['updated'])
	print "Airport: " + airport_name
	print "Status: " + status_reason
	print "Temperature: " + current_temp
	print "Time of most recent update: " + recent_update

# The following line of code tests print_airport():
print_airport('SFO')
print '---------------------------------'
print '---------------------------------'
print_airport('BWI')

# There are no other tests
print '---------------------------------'


# (5c) Iterate over the fav_airports list and print out the abbreviated info for
# each, by calling print_airport().

fav_airports = ['PIT', 'BOS', 'LGA', 'DCA']

# put your code here
for x in fav_airports:
	print print_airport(x)

# Error handling and exceptions
# (6a) Uncomment the bogus URL request below.  It should throw an exception.
#      This exception occurs when you request an invalid URL.  Wrap the 
#      urlopen() call, in the commented line below, in a try/except block similar to what was done in the
#      flickr example from class.
#      e.g. your block should catch urlib2.URLError exceptions, and print out
#      the appropriate reason or error code
print '---------------'
#x = urllib2.urlopen('http://www-personal.umich.edu/nonstudent')

# (6b) Define a function get_airport_safe().  It calls get_airport, but catches any errors that might occur (i.e., use try/except around the whole get_airport function call). If an error occurs, your function should print 'Error trying to retrieve airport.', followed by the reason or error code returned by the server, and return None.
print '---------------'
def get_airport_safe(K):
	m=get_airport(K)
	try:
		return urllib2.urlopen(m)
	except urllib2.URLError, e:
		if hasattr(e, 'reason'):
			print 'Error trying to retrieve airport.'
			print 'Reason: ', e.reason
		elif hasattr(e, 'code'):
			print 'The server couldn\'t fulfill the request.'
			print 'Error code: ', e.code
	return None
# put your code here
   
# Uncomment the following code to test get_airport_safe():
print get_airport_safe('xy')
print get_airport_safe('DTW')

print "---------------------"
#(6c) Now define a function print_airport_safe that calls get_airport_safe and, 
# if there's no error, prints the abbreviated data as in print_airport  

# Trying out your own airports (6d) Create a list including your 3 top airports
# and one that doesn't exist.  Print them out using the print_airport_safe()
# function.

# uncomment this code and fill in your_favs
#your_favs = []
#for a in your_favs:
#    print_airport_safe(a)


####---------------

# The rest of the homework is an EXTRA CREDIT opportunity.
# We strongly suggest you attempt this, since it will be very helpful as we move forward,
# and helpful for you as a programmer!

# It's time to retrieve data from another API!

# For that, create ANOTHER PYTHON FILE, called ps8application.py. 
# Do not put any more code in THIS file. If you do this part of the homework, it should be in a file with that name (ps8application.py).
# You will need to copy a few of the import statements from the top of this file.
# For this part of the assignment, you just have to demonstrate 
# that you can write a program that fetches data from some REST API other than Flickr 
# and the FAA site, and that it extracts any piece of meaningful information from the data 
# that comes back. 

# You can model this off the problems throughout the earlier part of this pset. 
# Think about how you need to adapt what we do in this pset to a new API.

# Feel free to do anything fun you want with this data in addition to extracting it and printing it,
# if you have ideas.

# Some possibilities that you might consider for an API to use for this:
#-- http://developer.rottentomatoes.com/docs
    # -- once you have registered and have a key, try http://developer.rottentomatoes.com/io-docs
#-- Or, feel free to pick your own!
# http://www.programmableweb.com/apis lists a ton of them. 
# Make sure to choose a REST API, not one of the other types.
# Also make sure that whatever API you choose has good, clear documentation like
# the rotten tomatoes example. Some APIs don't provide a lot of docs, and you want to avoid those.

# Note that you may need to provide an authentication key for some APIs, 
# like with the Flickr example in Thursday's class. It should be exactly the same idea. 
# Some services, like Twitter and Facebook, require more than just an authentication key; they require use of the oauth protocol. DON'T use one of those.

# WE REPEAT: DO NOT USE AN API THAT REQUIRES THE USE OF AUTH!!! You're probably not ready yet.

##Also note that when the FAA API is queried for an airport that doesn't exist, it gives a
#404 error. Some APIs that you may use will return JSON-formatted data saying
#that the requested item couldn't be found. You may have to check the contents
#of the data you get back to see whether a query was successful. 
# That's a step you didn't have to do with the FAA API. 
# So keep that in mind, read the docs, try a few examples of accessing a new API you choose...
