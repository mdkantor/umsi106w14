###################
import facebook
import json
import test

def pretty(obj):
    return json.dumps(obj, sort_keys=True, indent=2)

pos_ws = []
f = open('positive-words.txt', 'r')

for l in f.readlines()[35:]:
    pos_ws.append(unicode(l.strip()))
f.close()

neg_ws = []
f = open('negative-words.txt', 'r')
for l in f.readlines()[35:]:
    neg_ws.append(unicode(l.strip()))

# 1) Fill in the definition of the class Post to hold information about one post that you've made on Facebook.
    # Add to the __init__ method additional code to set the instance variables comments and likes. 
    # You need to pull out the appropriate data from the json representation of a single post. 
    # You can find a sample in the file samplepost.txt. There are tests that check whether you've pulled out the right data.
    
class Post():
    """object representing status update"""
    def __init__(self, post_dict):
        if 'message' in post_dict:
            self.message = post_dict['message']
        else:
            self.message = ""
        if 'comments' in post_dict:
        	#self.comments=post_dict['comments']
        	self.comments=post_dict['comments']['data']
        	#self.comments=list(post_dict['comments'])
        else:
        	self.comments= []
        if 'likes' in post_dict:
        	self.likes= post_dict['likes']['data']
        	#self.likes= list(post_dict['likes'])
        else:
        	self.likes= []
        # if the post dictionary has a 'comments' key, set self.comments to the
        # list of comment dictionaries extracted from the corresponding comments dictionary. 
        # Otherwise, set self.comments to an empty list []
        # Something similar has already been done for the contents of the original post, 
        # which is the value of the 'message' key in the dictionary, when it is present 
        # (photo posts don't have a message). 
        # See above for a model. But pulling out the list of dictionaries from a post_dict
        # is a little harder. Take a look at the sample of what a post_dict looks like
        # in the file samplepost.txt
     
        # Similarly, if the post dictionary has a 'likes' key, set self.likes to
        # the list of likes dictionaries from the corresponding likes dictionary.  
        # Otherwise, set self.likes to an empty list, []
         
     
    def emo_score(self):
		L1=[]
		p=self.message.split()
		for word in p:
			if word in pos_ws:
				L1.append(word)
			else:
				pass
		pos_total=len(L1)
		L=[]
		n=self.message.split()
		for word in n:
			if word in neg_ws:
				L.append(word)
			else:
				pass
		neg_total=len(L)
		return pos_total - neg_total
    
    
    def positive(self):
        L1=[]
        p=self.message.split()
        for word in p:
        	if word in pos_ws:
        		L1.append(word)
        	else:
        		pass
        pos_total=len(L1)
        return pos_total
        #return 0
                   
    def negative(self):
        L=[]
        n=self.message.split()
        for word in n:
        	if word in neg_ws:
        		L.append(word)
        	else:
        		pass
        neg_total=len(L)		
        return neg_total
        #return 0

		
sample = open('samplepost.txt').read()
sample_post_dict = json.loads(sample)
p = Post(sample_post_dict)
# use the next lines if you're having trouble getting the tests to pass.
# they will help you understand what a post_dict contains, and what
# your code has actually extracted from it and assigned to the comments 
# and likes instance variables.

try:
    test.testType(p.comments, 'list')
    test.testEqual(len(p.comments), 4)
    test.testType(p.comments[0], 'dictionary')
    test.testType(p.likes, 'list')
    test.testEqual(len(p.likes), 4)
    test.testType(p.likes[0], 'dictionary')
except:
    print "One or more of the test invocations is causing an error,\nprobably because p.comments or p.likes has no elements..."

# 2) In the Post class, fill in three methods:
    # -- positive() returns the number of words in the message that are in the list of positive words pos_ws (provided in our code)
    # -- negative() returns the number of words in the message taht are in the list of negative words neg_ws (provided in our code)
    # -- emo_score returns the difference between positive and negative scores
    
    # make the three tests pass as a way of checking that you have defined the methods correctly

p.message = "adaptive acumen abuses acerbic aches for everyone"
test.testEqual(p.positive(), 2)
test.testEqual(p.negative(), 3)
test.testEqual(p.emo_score(), -1)        
    
# 3) Get a json-formatted version of your last 100 posts on Facebook. 
# (Hint: use the facebook module, as presented in class, and use https://developers.facebook.com/tools/explorer to get your access token. 
# Every couple hours you will need to get a new token.)




access_token = 'CAACEdEose0cBABbAssmTr1qk2de2maVLQ0VZA84paZCBZBdRebA4Ge0txza4wzOMRPgXPmhSW30qUQ4WnZAPdhJgNSglA5AZAMGHP2tIIyZC2yBxtAGJDNYHZBaJVZCVGDGwKOXUCJAn3ZC3APuZA2qQUoJZBDRxm7G7vNMoofnawVvoUzXAqmjji4yppabRq74YZANS1ZAzbbe5FrwZDZD'

graph = facebook.GraphAPI(access_token)
feed = graph.get_object("me/feed?limit=100")

my_feed= pretty(feed)
feed_file= open("my_feed_fb.json", "w")
feed_file.write(my_feed)
feed_file.close()

# 4) For each of those posts
    # -- create an instance of your class Post, filling in attributes for:
        #-- the message if it exists
        #-- the comments data if it exists
        #-- the likes data if it exists
posts= []
for p in feed['data']:
	posts.append(Post(p))
print posts
# 5) Compute the top three likers and commenters on your posts 
# (the people who did the most comments and likes)

d={}
for p in posts:
	for like in p.likes:
		name=like['name']
		if name not in d:
			d[name]=1
		else:
			d[name]=d[name] + 1
			likers= sorted(d.items(), None, lambda x:x[1], True)
print likers[:3]

d2={}
for p in posts:
	for commenter in p.comments:
		name=commenter['from']['name']
		if name not in d2:
			d2[name]=1
		else:
			d2[name]=d2[name]+1
			commenters= sorted(d2.items(), None, lambda x:x[1], True)
print commenters[:3]

commenterzs=dict(commenters)
commenters_values=commenterzs.values()
print commenters_values

likerzs=dict(likers)
likers_values=likerzs.values()
print likers_values

 
# 6) Were there more distinct commenters or likers? 
# (Just for fun, not for credit: find the people who commented on at least 
# one post but never liked any of them.)



# 7) Output a .csv file that lets you make scatterplots showing 
# net positivity on x-axis and comment-counts and like-counts on y-axis. 
# Can you see any trend in whether your friends are more likely 
# to respond to positive vs. negative posts? 
# Post a screenshot of your scatterplot to the facebook group.

n_file=open("pos_graph.csv", "w")
n_file.write("emo_score, comment_count, like_count\n")
for x in posts:
	r="%d, %d, %d\n" % (x.emo_score(), len(x.comments), len(x.likes))
	n_file.write(r)
n_file.close()

