class Silly:
   """This class does nothing, but we use it to
 illustrate instances and members"""

print "Silly has type ", type(Silly)
x1 = Silly()
print "x1 has type ", type(x1)
x1.foo = 3
print x1.foo
print x1.__class__.__name__
print x1.__class__ == Silly
x2 = Silly()
x2.foo = 4
print x2.foo
print x1.foo
print Silly.__name__
# What will happen if you uncomment the following?
#print Silly.foo

class Dog:
    """A slobbering friend"""

    def __init__ (self, n, barktype="woof"):
        """Initialize with number of woofs"""
        self.woofcount = n 
        self.barktype= barktype  #right side is meow or arf or woof and we are bounding it to the instance
    def bark (self):
        """Bark out loud"""
        for i in range(self.woofcount):
            print self.barktype
    def __str__(self):
        """Called whenever a printed representation of self is needed"""
	return "Barks %d times" % self.woofcount
    

print "Dog has type", type(Dog)
print "Dog.bark has type", type(Dog.bark)

Iorek = Dog(23)
print type(Iorek)
print Iorek.__class__.__name__
print Iorek.__class__ == Dog
print Iorek.woofcount

Woofus = Dog(1)
print Woofus.woofcount
Woofus.bark()
print "-----"
Woofus.woofcount = 2
print Woofus.woofcount
Woofus.bark()


Iorek.bark()

Woofus.__str__()
Fluffy = Dog(1, "Ruff") #1stthing it does is call ___ then it calls the inite method
Woofus = Dog(3, "Woof")
Rover = Dog(4, "Arf")
Meep = Dog(7, "Meeeeeeep")

for d in [Fluffy, Woofus, Rover, Meep]:
	d.bark()
	print "---"

class Point:
    """ Point class for representing and manipulating x,y coordinates. """

    def __init__(self, initX, initY):

        self.x = initX
        self.y = initY

    def getX(self):
        return self.x

    def getY(self):
        return self.y

    def distanceFromOrigin(self):
        return ((self.x ** 2) + (self.y ** 2)) ** 0.5
    
    def move(self, dx, dy):
        self.x = self.x + dx
        self.y = self.y + dy
        
    def __str__(self):
        return str(self.x)+","+str(self.y)


p = Point(7,6)
print(p)
p.move(5,10)
print(p)
    

