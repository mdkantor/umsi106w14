things = [3, 5, -4, 7]
# write some code that applies map to things, 
# passing different functions for transforming the
# items

def dubbdown(value):
	return 0.5*value

def dubbdownnums(a_list):
	n_list= map(dubbdown, a_list)
	return n_list

thingsdubb= dubbdownnums(things)
print thingsdubb 


print map(lambda value: 5*value, things)

def lengths(strings):
	new_strings= map(lambda s: len(s), strings)
	return new_strings
    #"""lengths takes a list of strings as input and returns a list of numbers that are the lengths
   # of strings in the input list. Use map!"""
  # fill in this function's definition to make the test pass.

t= ["Hello", "hi", "bye"]
print lengths(t)  

import test
test.testEqual(lengths(["Hello", "hi", "bye"]), [5, 2, 3])

# write some expressions using the filter function 
#Return a shorter list of strings containing only the strings with more than four characters. Use the filter function."""

# write your code here
def longwords(strings):
	new_lst= filter(lambda value: len(value) >4, strings)
	return new_lst
	
	

#1) using filter fxn & lambda
def one_longwords(strings):
	new_lst= filter(lambda value: len(value) >4, strings)
	return new_lst
#2) using filter fxn & created a new def fxn(no lambda)
def two_longwords(strings):
	def long_enough(s):
		return len(s)>4
	return filter(long_enough, strings)
#3) using list comprehension
def three_longwords(strings):
	return [s for s in strings if len(s) >4]
##[s for s in strings]=....saying "return (the)strings for each string in all of
###the list of strings that fit the conditional after the IF statement ( if len(s)>4)  


	
r= ["Hello", "hi", "bye", "wonderful"]
print "XXXXXXXXXXXXXXXX"
print one_longwords(r)    
print "XXXXXXXXXXXXXXXX"
print two_longwords(r) 
print "XXXXXXXXXXXXXXXX"   
print three_longwords(r)    
test.testEqual(longwords(["Hello", "hi", "bye", "wonderful"]), ["Hello", "wonderful"])

#### exercises on list comprehensions

# rewrite the functions lengths using a list comprehension
def lengths_2(strings):
	nn=[len(r) for r in strings]
	return nn
print "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
print lengths_2(t)
# rewrite the function longwords using a list comprehension
def three_longwords(strings):
	return [s for s in strings if len(s) >4]

# combine them in a single function, using a single list comprehension, 
##that returns the lengths of those strings that have at least 4 characters.
def combo(strings):
	return [len(s) for s in strings if len(s) >4]

print combo(r)


##### exercise using reduce

# write a function that takes a list of numbers and returns the sum of the squares of all the numbers. Use reduce.
nums= [3, 4, 6]
print reduce(lambda x,y: x + y*y, nums, 3)
# Now write an alternative version of that same function that uses map to get a list of the squared values and then just uses the built-in sum function, rather than using reduce."""