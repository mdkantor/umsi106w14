##in class practice stuff
things= [3, 5, -4, 7]
def times5(num):
	return num*5
	
print map(times5, things)
print map(lambda value: value*5, things)

#REDUCE type problems

##write a fxn that takes a list of numbers and returns
###the sum of the squares of all the numbers. Use REDUCE.
nums= [3, 2, 2, -1, 1]

print reduce(lambda x, y: x + y*y, nums, 3)

def reducer(accum, item):
	return accum + item*item
print reduce(reducer, nums, 3)

##now write an alternative version of that same fxn that uses map
###to get a list of the squared values&&then just get a list of the
