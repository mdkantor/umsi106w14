import urllib
import urllib2
import json
    
def pretty(obj):
    return json.dumps(obj, sort_keys=True, indent=2)

base="http:foo.com"
d = {'format':'json', 'p2':'hi there: yes'}
print base + "?" + urllib.urlencode(d)

def safeGet(url):
    try:
        return urllib2.urlopen(url)
    except urllib2.URLError, e:
        if hasattr(e, 'reason'):
            print 'We failed to reach a server.'
            print 'Reason: ', e.reason
        elif hasattr(e, 'code'):
            print 'The server couldn\'t fulfill the request.'
            print 'Error code: ', e.code
    return None

# apply for a flickr authentication key at http://www.flickr.com/services/apps/create/apply/?
# paste the key (not the secret) as the value of the variable flickr_key
flickr_key = 'de1b720393a1801e62b381561d08ee8b'
    
def flickrREST(baseurl = 'http://api.flickr.com/services/rest/', 
    method = 'flickr.photos.search', 
    api_key = flickr_key,
    format = 'json',
    params={},
    printurl = False
    ):
    params['method'] = method
    params['api_key'] = api_key
    params['format'] = format
    url = baseurl + "?" + urllib.urlencode(params)
    if printurl:
        print url
        return None
    else:
        return safeGet(url)

print flickrREST(params = {'tags':'beach', 'per_page':10}, printurl = True)
## should produce something like this, with your key http://api.flickr.com/services/rest/?per_page=10&format=json&api_key=yourkeyhere&method=flickr.photos.search&tags=beach
try:
   result = flickrREST(params = {'tags':'beach', 'per_page':10})
   print result.read()
except:
   print "Error calling FlickREST"

import webbrowser
def flickrdemo():    
    result = flickrREST(params = {'tags':'beach', 'per_page':5})
    txt = result.read()
    print txt[0:14]  # there's some junk at the beginning, rest is json
    jsresult = txt[14:-1]
    d = json.loads(jsresult)
    print pretty(d)
    print d.keys()
    print d['photos'].keys()
    photos = d['photos']['photo']
    print len(photos)
    for k in photos[0]:
        print k
    for photo in photos:
        owner = photo['owner']
        pid = photo['id']
        url = 'http://www.flickr.com/photos/%s/%s' % (owner, pid)
        webbrowser.open(url)

try:
   flickrdemo()
except:
   print "Error in flickrdemo()"
