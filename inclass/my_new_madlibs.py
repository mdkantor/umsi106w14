import nltk
import random

# create mad lib from file
tagset = ['NN', 'NNS', 'VB', 'JJ'] # these are the parts of speech we care about for now
fname = raw_input("What's the file you want to turn into a madlib?\nEnter the full name.\n")
tagmap = {"NN":"noun","NNS":"plural noun","VB":"verb","JJ":"adjective"}


f = open(fname, 'r')
fstr = f.read()
tokenized_file = nltk.word_tokenize(fstr)
tagged_file = nltk.pos_tag(tokenized_file) # gives us a tagged list of tuples

# take out a few random words of each tag type
nouns = [(n,t) for (n,t) in tagged_file if t == 'NN']
pnouns = [(n,t) for (n,t) in tagged_file if t == 'NNS']
verbs = [(n,t) for (n,t) in tagged_file if t == 'VB']
adjs = [(n,t) for (n,t) in tagged_file if t == 'JJ']

all_types = [nouns, pnouns, verbs, adjs] # list of lists of tuples

def pick_random_words(wordlist,number):
	num = len(wordlist)
	indexes = [random.choice(range(num)) for x in range(number)]
	words = [wordlist[i][0] for i in indexes]
	return words

for t in all_types:
	wl = pick_random_words(t,2)
	for w in wl:
		fstr = fstr.replace(w,t[0][1]) # brittle -- this can be made better

# play madlib
i = 0
split_text = nltk.word_tokenize(fstr) # retokenize the madlibbed text
for wd in split_text:
	if wd in tagset:
		new_word = raw_input("Please enter a %s:\n" % (tagmap[wd]))
		split_text[i] = new_word
	i += 1

print " ".join(split_text)
