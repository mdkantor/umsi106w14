# -*- coding: latin-1 -*-

nested1 = [['a', 'b', 'c'],['d', 'e'],['f', 'g', 'h']]
nested2 = [1, 'hi', ['a', 'b', 'c'],['d', 'e'],['f', 'g', 'h']]
nested3 = {'k1': [1, 2, 3], 'k2':{'subk1': 4}}


print "----nested1----"
print len (nested1)
for x in nested1:   
   print type(x)
   print len(x)
   print x[0]


print "----nested2----"
nested2 = [1, 'hi', ['a', 'b', 'c'],['d', 'e'],['f', 'g', 'h']]
print len (nested2)
for x in nested2:
   print type(x)
   if type(x)=='list':
       print len(x)
       print x[0]
   else:
       print x

print "---nested iteration---"
nested1 = [['a', 'b', 'c'],['d', 'e'],['f', 'g', 'h']]
for x in nested1:
   print "level1: "
   for y in x:
       print "\tlevel2: " + y
     

import testimport
print testimport.y
print testimport.square(3)

import testimport as t
print t.y
print t.square(4)

from testimport import y
print y

import json
def pretty(obj):
    return json.dumps(obj, sort_keys=True, indent=2)

# download the file fbdata.json from cTools, resources/Slides folder
posts = json.load(open('fbdata.json', 'r'))['data']
#nespost= json.load(open('nested.json', 'r'))

mypost = posts[0]
mypost2 = posts[11]
mypost3 = posts[23]
#print mypost2
#print pretty(mypost2)


### save just mypost2 is another file, because it's easier to work with data for just one post.
f = open('nested.json', 'w')
f.write(pretty(mypost2['comments']))
f.close()

comments = json.load(open('nested.json', 'r'))
print comments.keys()

print type(comments['data']) 
print len(comments['data']) #this finds out how many items are in data key
print type(comments['data'][0]) #this finds type of 1st item in list, need 2 do this becuase data is nestled
print pretty(comments['data'][0]) #this finds 1st item in dict/nestled whatever ITEMS
print comments['data'][0]['message']

print "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
for comment in comments['data']:
   print "----next comment----"
   for k in comment.keys():
       print "key is %s; value is %s" % (k, comment[k])

try:
    x = 1/0
except ArithmeticError, detail:
    print 'Handling run-time error:', detail
print "still running, after error handled!"
print "hooray!"
#x =1/0

try:
    for i in range(5):
        print 1.0/(3-i)
except Exception, error_instance:
    print "Got an error", error_instance

import urllib2

def try_url_demo(url):
    print '\ntrying to fetch', url
    try:
        response = urllib2.urlopen(url)
    except urllib2.URLError, e:
        if hasattr(e, 'reason'):
            print 'We failed to get the file.'
            print 'Reason: ', e.reason
        elif hasattr(e, 'code'):
            print 'The server couldn\'t fulfill the request.'
            print 'Error code: ', e.code
    except ValueError, e:
        print e
    else: #this else goes with try/except
        # everything is fine; got the page
        print "got", response.geturl()

try_url_demo('http://presnick.people.si.umich.edu/nofile.hm')
try_url_demo('http://www.nodomain/index.htm')
try_url_demo('http://presnick.people.si.umich.edu/')


  
def safeGet(url):
    try:
        return urllib2.urlopen(url)
    except urllib2.URLError, e:
        if hasattr(e, 'reason'):
            print 'We failed to reach a server.'
            print 'Reason: ', e.reason
        elif hasattr(e, 'code'):
            print 'The server couldn\'t fulfill the request.'
            print 'Error code: ', e.code
    return None
