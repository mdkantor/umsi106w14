#####Part I. Describe your project #####

# Include:
# a.	What data sources you will use
# b.	How you plan to process the data you will get.
# c.	How you will present the results.
s = """For my project I am going to pull tweets from search results in Twitter that mention Beyonce & Jay-Z. I am going to use the data to compile different things about these two sources, including calculating the top topics (songs/events) discussed in these specific tweets, and calculate a Jay-to-B ratio to see how often each other are mentioned in tweets together. I am going to present my data in some graph that I will have created through excel."""
print s
print
print "------"

#####Part II: Describe a class you willl define ####

# The name of my class will be...
your_name = "illuminati "

# Each instance of my class will represent one...
your_inst_represents = "tweet"

# Each instance of my class will have ... instance variables
your_inst_var_count = 3

# Each instance will have instance variables that keep track of...
your_inst_vars = "jay-z score, beyonce score, top songs score"

# One method of my class, other than __init__, will be named...
your_method_name = "jay-z score"

# When invoked, that method will...
your_method_description = "generate the score of how connected that tweet is to Jay-Z"

print "The name of my class will be %s. Each instance of my class will represent one %s. Each instance will have %d instance variables. The instance variables will keep track of %s. One method of my class, other than __init__, will be named %s. When invoked, that method will %s." % (your_name, your_inst_represents, your_inst_var_count, your_inst_vars, your_method_name, your_method_description)
print
print "----------"

#####Part III: Code to retrieve data and extract something from it #####
import tweepy
import webbrowser
import sys

##Paste your API_KEY and API_SECRET from the app page for the app you created on apps.twitter.com
API_KEY = "FA19EhqmMbnIurq58WRrpXYBk"
API_SECRET = "TqHsmTqlMNmc52nZ4c9ckW9P7otwYSNOycKndsovNWLEJCVKoJ"

auth = tweepy.OAuthHandler(API_KEY, API_SECRET)
try:
    redirect_url = auth.get_authorization_url()
except tweepy.TweepError:
    print 'Error! Failed to get request token.'
webbrowser.open(redirect_url)
verifier = raw_input('Please input the verifier that you get after logging in in the browser window that just opened: ')
try:
    auth.get_access_token(verifier)
except tweepy.TweepError:
    print 'Error! Failed to get access token.' 
#See requirements in the "Final Project Instructions" document in cTools
api = tweepy.API(auth)
search_beyo=api.search('beyonce')
for tweet in search_beyo:
	print tweet.text.encode(sys.stdout.encoding, 'replace')

search_hova=api.search('jay z')
for tweet in search_beyo:
	print tweet.text.encode(sys.stdout.encoding, 'replace')