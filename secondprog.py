print "Hello world"

def make_incrementer(n): #fxns as returned values
	return lambda x:x+n
x= make_incrementer(42)
print x(2)

def apply_fn(f,x): #fxns are paramenter value
	return f(x)
print apply_fn(abs, -3)
print apply_fn(lambda x: x*x, 3)

#this one puts errything together
print apply_fn(make_incrementer(3), 10) #should get 13?

#simple use of urllib2
##this example is opening code from BetchesLoveThis and printing
###out some stuff

import urllib2
betchesread=urllib2.urlopen('http://www.betcheslovethis.com/')
t=betchesread.read()
print t[:300]
print "characters:", len(t)
print "lines:", len(betchesread.readlines())
print "url is:", betchesread.geturl()
print "info:"
print betchesread.info()

##figure this out. i am trying to import code from another file trythis.py i made
import trythis
print trythis.zz
print trythis.numbr(5,2)
