import random
import test

def letter_frequencies(txt, alphabet):
    d = {}
    for c in alphabet:
        d[c] = 0
    for c in txt:
        if c in alphabet:
            d[c] = d[c] + 1
    return d
    
##### guessers now return an ordered list of guesses for the next letter, consisting of all the letters in the alphabet, in some order.
def count_guesses(next_letter, guesses):
    try:
        # print "%s took %d guesses from %s" % (next_letter, guesses.index(next_letter) + 1, "".join(guesses))
        return guesses.index(next_letter) + 1
    except:
        print "%s is not among the guesses made; guesses should contain all the alphabet letters, in some order.\n\t%s" % (next_letter, guesses)

def game(txt, guesser):
    """Plays one game"""
    # accumulate the text that's been revealed
    revealed_text = ""    
    # accumulate the total guess count
    total_guesses = 0
    # accumulate the total characters to be guessed
    total_chars = 0
    
    # Loop through the letters in the text, making a guess for each
    for c in txt:
        if c in alphabet: # skip letters not in our alphabet; don't have to guess them
            guesses = guesser(revealed_text)
            total_chars += 1
            total_guesses += count_guesses(c, guesses)
            revealed_text += c
    return total_chars, total_guesses

def guess_by_frequency(prev_txt):
    return letters_sorted_by_frequency
    
def add_prefix(pre, guesses, pri=2):
    if pre in heuristics:
        # add any new guesses
        for guess in guesses:
            if guess not in heuristics[pre]['guesses']:
                heuristics[pre]['guesses'].append(guess)
    else:
        heuristics[pre] = {'priority' : pri, 'guesses':guesses}
    
def add_word(w, heuristics, pri = 2):
    """Takes a word w as input and adds all its prefixes of length > 2 to the 
    heuristics dictionary"""
    for i in range(len(w)-2):
        prefix = w[:i+2]
        next_let = w[i+2]
        add_prefix(prefix, [next_let], pri)
    # at the end of the word, guess a space, period, or comma.
    add_prefix(w, [' ', '.', ','], pri)

def reset_heuristics(heuristics, sorted_caps, sorted_letters):
    heuristics.clear()
    heuristics['q'] = {'priority': 1, 'guesses':['u', 'a']}
    heuristics['. '] = {'priority': 2, 'guesses': sorted_caps}
    heuristics[''] = {'priority':5, 'guesses': sorted_letters}

alphabet = " !#$%&()*,-./0123456789:;?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[]abcdefghijklmnopqrstuvwxyz\'\""
caps = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

f = open('train.txt', 'r')
overall_freqs = letter_frequencies(f.read(), alphabet)

# Sort the letters in descending ordering of frequency
sorted_letters = sorted(overall_freqs.keys(), key = lambda k: overall_freqs[k], reverse = True)

test.testEqual(sorted_letters[:3], [' ', 'e', 't'])

# write a filter or list_comprehension to get the subset of sorted_letters that are in caps
sorted_caps = [c for c in sorted_letters if c in caps]

test.testEqual('D' in sorted_caps, True)
test.testEqual('d' in sorted_caps, False)

heuristics = {}
reset_heuristics(heuristics, sorted_caps, sorted_letters)

# You will need to fill in code for the new heuristic_guesser function. In previous problem sets, this function returned a 'next guess'. In this problem set it returns an ordered list of guesses to make.
# After each of the comments, write the code that implements the comment, and make the test pass.
def heuristic_guesser(prev_txt, heuristics=heuristics, tests=False):
    if tests:
        prev_txt = "End of sentence. Period. "
    # keep only those endings that match prev_txt
    # use a filter or list comprehension
    # hint: use the endswith method to see if prev_txt ends with the key 
    endings = [e for e in heuristics.keys() if prev_txt.endswith(e)]
    if tests:
        test.testEqual(len(endings), 2, "Should have two matching endings.")
        
    # sort those keys by priority
    endings = sorted(endings, key= lambda ending: heuristics[ending]['priority'])
    if tests:
        test.testEqual(endings, ['. ', ''])
    
    # collect a list of the guess-lists associated with each of those keys; use map or a list comprehension
    guess_lists = [heuristics[e]['guesses'] for e in endings]
    if tests:
        test.testEqual(guess_lists, [sorted_caps, sorted_letters], "guess_lists")
    # merge all those guess lists to make a single list of guesses that has no duplicaets
    accumulated_guesses = []
    for l in guess_lists:
        new_guesses = [g for g in l if g not in accumulated_guesses]
        accumulated_guesses = accumulated_guesses + new_guesses
    if tests:
        test.testEqual(accumulated_guesses, ['I', 'H', 'T', 'S', 'A', 'W', 'M', 'B', 'Y', 'O', 'C', 'E', 'N', 'L', 'P', 'R', 'G', 'D', 'F', 'J', 'V', 'U', 'K', 'Q', 'X', 'Z', ' ', 'e', 't', 'a', 'o', 'n', 'h', 'i', 's', 'r', 'd', 'l', 'u', 'm', 'w', 'c', 'y', 'f', 'g', ',', 'p', '.', 'b', '"', 'v', 'k', "'", '-', '?', 'x', 'j', 'q', '!', ';', 'z', '1', '0', ':', '8', '2', '4', '6', '*', '5', '/', '3', ')', '(', '7', '9', '&', '$', '@', '#', '%', '[', ']'], "acccumulated_guesses")
    
    # return the guesses
    return accumulated_guesses

heuristic_guesser("", tests=True)



f = open('train.txt', 'r')
train = f.read()
f.close()

f= open('test.txt', 'r')
test_txt = f.read()
f.close()

res1 = game(test_txt, heuristic_guesser)

# Now use the text in the variable train to calculate the most frequent words in that text. Only consider words that have more than 4 letters.
# Call the add_word function on each of the 20 most frequent words.
# Then see how much your heuristic_guesser has improved.

words = {}
for w in train.split():
    if len(w) >=4:
        if w not in words:
            words[w] = 1
        else:
            words[w] = words[w] + 1

sorted_words = sorted(words.items(), None, lambda x: x[1])

for (word, count) in sorted_words[-20:]:
    add_word(word, heuristics)

res2 = game(test_txt, heuristic_guesser)

def output_result(min_guesses, actual_guesses):
    print "Minimum guesses: %d\tActual guesses: %d" % (min_guesses, actual_guesses)

output_result(res1[0], res1[1])
output_result(res2[0], res2[1])

print "The improved game made %d less guesses. " % (res1[1] - res2[1])


# Figure out most surprising facebook post
import facebook

access_token = "CAACEdEose0cBAGQOtbnnr5u0VT5Mycw3OfCzrh0rWMl8dLVHTaKLal1UZAROCrtIR4rSzxSFVh1L8pP4XoX8VO9iFNpniUNOZCeo1cNh2Ll5YpcVIrB50AsFKSO3wA2CGPEPdV9zDzOcsjlUUZB4gyy2tD0hSa3wl0lCjLhHVvPUu4vrpsLBpeKuXT750gZD"
graph = facebook.GraphAPI(access_token)
feed = graph.get_object("245188182322906/feed?limit=150")

posts = [p for p in feed['data'] if 'message' in p]


def find_redundancy(post):
    #if retraining heuristics on the other posts
    # reset_heuristics(heuristics, sorted_caps, sorted_letters)
    # for p in posts:
        # if p != post:
            # for w in p['message'].split():
                # add_word(w, heuristics)
    
    message = post['message']
    min_guesses, actual_guesses = game(message,heuristic_guesser)
    redundancy = actual_guesses/float(min_guesses)
    post['redundancy'] = redundancy
    return post


### if retraining heuristics on the letter frequencies from the posts
# overall_freqs = letter_frequencies("".join(p['message'] for p in posts), alphabet)
# sorted_letters = sorted(overall_freqs.keys(), key = lambda k: overall_freqs[k], reverse = True)
# sorted_caps = [c for c in sorted_letters if c in caps]
# reset_heuristics(heuristics, sorted_caps, sorted_letters)

for p in posts:
    find_redundancy(p)

surprising_posts = sorted(posts,key=lambda p: p['redundancy'], reverse=True)

print "-----Top 10 most surprising-----"
for p in surprising_posts[:10]:
    print "%.2f: %s" % (p['redundancy'], p['message'])

print "\n----most predictable posts------"
for p in surprising_posts[-10:]:
    print "%.2f: %s" % (p['redundancy'], p['message'])
